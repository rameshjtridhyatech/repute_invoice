<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Kaira Infotech | Invoice Management
 *
 * A simple and powerful web app based on PHP CodeIgniter framework manage invoices.
 *
 * @package Kaira Infotech | Invoice Management
 * @author  Hitesh Yadav
 * @copyright   Copyright (c) 2020
 * @since   Version 1.6.0
 * @filesource
 */

class MY_Controller extends CI_Controller {
    public function __construct() {
        parent::__construct();
        //get your data
        $csrf = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );
        $global_data = array('csrf'=>$csrf);
        $this->load->vars($global_data);
    }
}
?>
